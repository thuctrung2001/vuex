import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
const storeData = {
  state: {
    todos: [
      { id: 1, title: "To do 1", completed: true },
      { id: 2, title: "To do 2", completed: true },
      { id: 3, title: "To do 3", completed: false }
    ],
    auth: {
      isAuthenticated: true
    }
  }
};

const store = new Vuex.Store(storeData);

export default store;
